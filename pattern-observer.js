Event = function(){
    this._observers = [];
}

Event.prototype ={
    raise: function (data){
        console.log(this._observers);
        for (var i in this._observers){
            var item = this._observers[i];
            item.observer.call(item.context, data);
        }
    },
    subscribe: function (observer, context){
        var ctx = context || null;
        this._observers.push({ observer, context: ctx });
    },
    unsubscribe: function (observer, context){
        for (var i in this._observers){
            if (this._observers[i].observer == observer && this._observers[i].context == context){
                    this._observers.splice(i, 1)
            }
        }
    }
} 

var someEvent = new Event();

someEvent.subscribe(function (data){
    console.log("wohoooooo " + data)
});

var someObject = {
    _topSecretInfo: 42,
    observerFunction: function () {
        console.log("Top Secret:" + this._topSecretInfo)
    }
}
someEvent.subscribe(someObject.observerFunction, someObject); 
someEvent.raise("yeaah!");
// someEvent.unsubscribe(someObject.observerFunction, someObject);
// someEvent.raise("yeaah!");