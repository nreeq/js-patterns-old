function Ball(params){
    this._radius = params.radius
    this._color = params.color
}

Ball.prototype = {
    constructor: Ball,
    INCREMENTATION_STEP: 5,
    draw: function(){
        console.log(`ball drawn with radius: ${this._radius} and color: ${this._color}`)
    },
    inc: function(){
        this._radius += this.INCREMENTATION_STEP
    }
}

function StripedBall( ball ){
    this._ball = ball
}

StripedBall.prototype = {
    constructor: StripedBall,
    draw: function(){
        this._ball.draw()
        console.log("and with stripes")
    },
    inc: function(){
        return this._ball.inc()
    }
}

function SpleckledBall ( ball ){
    this._ball = ball
}

SpleckledBall.prototype = {
    constructor: SpleckledBall,
    draw: function(){
        this._ball.draw()
        console.log("and with dots");
    },
    inc: function(){
        return this._ball.inc()
    }
}

const ball1 = new SpleckledBall( new StripedBall( new Ball({ radius: 150, color: 'red' })))
const ball2 = new StripedBall( new SpleckledBall( new Ball({ radius: 200, color: 'green' })))

ball1.draw()
ball1.inc()
ball1.inc()
ball1.draw()